<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
});

Route::get('/posts', 'SocialNetwork\PostController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
