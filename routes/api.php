<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->group(function () {
    Route::prefix('user')->group(function () {
        Route::get('/', function (Request $request) {
            return $request->user();
        });

        Route::namespace('SocialNetwork')->group(function () {
            Route::get('/posts', 'UserPostController@index');
        });
    });

    Route::namespace('SocialNetwork')->group(function () {
        Route::resource('posts', 'PostController');
        Route::put('posts/{post}/like', 'LikePostController@update');
        Route::resource('posts/{post}/comments', 'CommentPostController');
        Route::resource('friends', 'FriendController');
        Route::post('profile/edit', 'ProfileController@update');
        Route::patch('password/edit', 'UpdatePasswordController@update');
    });
});

Route::post('auth/login', 'SocialNetwork\LoginController@login');
Route::post('auth/register', 'SocialNetwork\RegisterController@store');
