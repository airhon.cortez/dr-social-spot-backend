<?php

namespace App\Repositories\Interfaces;

interface FriendRepositoryInterface {
    public function friends();
}
