<?php

namespace App\Repositories\Interfaces;

interface ProfileRepositoryInterface
{
    public function update($request);

    public function resetPassword($request);
}
