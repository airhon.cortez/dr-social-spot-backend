<?php

namespace App\Repositories\Interfaces;

interface CommentRepositoryInterface
{
    public function comment($request);

    public function store($post);

    public function show($comment);

    public function update($comment);

    public function delete($comment);
}
