<?php

namespace App\Repositories\Interfaces;

interface PostRepositoryInterface
{
    public function all();

    public function userPosts();

    public function attributes($request);

    public function store();

    public function show($post);

    public function update($post);

    public function delete($post);

    public function likePost($post);
    
    public function unlikePost($post);
}
