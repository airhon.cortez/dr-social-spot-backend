<?php

namespace App\Repositories;

use App\Models\Post;
use App\Repositories\Interfaces\PostRepositoryInterface;
use Illuminate\Support\Str;

class PostRepository implements PostRepositoryInterface
{
    public $fields;

    public function all()
    {
        return Post::with('comments', 'user', 'postLikes')->get();
    }

    public function userPosts()
    {
        return auth()->user()
            ->posts()
            ->with('comments', 'postLikes')
            ->get();
    }

    public function attributes($request)
    {
        $this->fields = [
            'title'     => $request->title,
            'content'   => $request->content,
            'image'     => $this->fileUpload($request),
        ];

        return $this;
    }


    public function store()
    {
        return auth()->user()
            ->posts()
            ->create($this->fields);
    }

    public function show($post)
    {
        return auth()->user()
            ->posts()
            ->findOrFail($post->id);
    }

    public function update($post)
    {
        $post = auth()->user()
            ->posts()
            ->findOrFail($post->id);

        $post->update($this->fields);

        return $post;
    }

    public function delete($post)
    {
        return auth()->user()
            ->posts()
            ->findOrFail($post->id)
            ->delete();
    }

    public function likePost($post)
    {
        $post->increment('likes');

        auth()->user()->postLikes()->create([
            'post_id' => $post->id
        ]);
    }

    public function unlikePost($post)
    {
        $post->decrement('likes');

        auth()->user()
            ->postLikes()
            ->liked($post)
            ->first()
            ->delete();
    }


    protected function fileUpload($request)
    {
        if (!$request->file('image')) return null;

        $file = $request->file('image');

        $filename = Str::random(40) . '.' . $file->extension();

        $file->storeAs('posts/' . auth()->user()->id, $filename, 'public');

        return $filename;
    }
}
