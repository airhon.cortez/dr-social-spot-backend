<?php

namespace App\Repositories;

use App\Repositories\Interfaces\CommentRepositoryInterface;

class CommentRepository implements CommentRepositoryInterface
{
    public $content;

    public function comment($request)
    {
        $this->content = $request->content;

        return $this;
    }

    public function store($post)
    {
        return auth()->user()
            ->comments()
            ->create([
                'post_id' => $post->id,
                'content' => $this->content
            ]);
    }

    public function show($comment)
    {
        return auth()->user()
            ->comments()
            ->findOrFail($comment->id);
    }

    public function update($comment)
    {
        $comment = auth()->user()
            ->comments()
            ->findOrFail($comment->id);

        $comment->update(['content' => $this->content]);

        return $comment;
    }

    public function delete($comment)
    {
        return auth()->user()
            ->comments()
            ->findOrFail($comment->id)
            ->delete();
    }
}
