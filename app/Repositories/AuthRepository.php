<?php

namespace App\Repositories;

use App\Http\Resources\LoginResource;
use App\Models\User;
use App\Repositories\Interfaces\AuthRepositoryInterface;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthRepository implements AuthRepositoryInterface
{

    public function create($request)
    {
        $new_user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return $this->addFriend($new_user);

    }

    private function addFriend($new_user)
    {
        User::get()->map(function ($user) use ($new_user) {
            if ($user->id !== $new_user->id) {
                $new_user->friends()->create([
                    'friend_id'     => $user->id,
                    'status'        => 'accepted',
                    'accepted_at'   => now(),
                ]);
            }
        });

        $token = $new_user->createToken(Str::random(40))->plainTextToken;

        return explode("|", $token)[1];
    }
}
