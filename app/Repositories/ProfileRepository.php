<?php

namespace App\Repositories;

use App\Repositories\Interfaces\ProfileRepositoryInterface;
use Illuminate\Support\Str;

class ProfileRepository implements ProfileRepositoryInterface
{
    public function update($request)
    {
        auth()->user()->update([
            'name'      => $request->name,
            'email'     =>  $request->email,
            'image'     => $this->fileUpload($request),

        ]);

        return auth()->user();
    }


    public function resetPassword($request)
    {
        auth()->user()->update([
            'password' => bcrypt($request->password)
        ]);

        return auth()->user();
    }

    protected function fileUpload($request)
    {
        if (!$request->file('image')) {
            if (auth()->user()->image)
                return explode("/", auth()->user()->image)[6];
            
            return null;
        };

        $file = $request->file('image');

        $filename = Str::random(40) . '.' . $file->extension();

        $file->storeAs('avatars/' . auth()->user()->id, $filename, 'public');

        return $filename;
    }
}
