<?php

namespace App\Repositories;

use App\Repositories\Interfaces\FriendRepositoryInterface;

class FriendRepository implements FriendRepositoryInterface
{
    public function friends()
    {
        return auth()->user()
            ->friends()
            ->accepted()
            ->get()
            ->map->format();
    }
}
