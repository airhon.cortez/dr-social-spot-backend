<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    protected $guarded = [];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)->with('user');
    }

    public function postLikes()
    {
        return $this->hasMany(PostLike::class)->with('user');
    }

    public function getImageAttribute($value)
    {
        if ($value) {
            return  Storage::disk('public')
                ->url('posts/' . auth()->user()->id . '/'. $value);
        }

        return null;
    }
}
