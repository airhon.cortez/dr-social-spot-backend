<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{
    protected $guarded = [];

    public function scopeAccepted($query)
    {
        $query->whereStatus('accepted');
    }

    public function userFriends()
    {
        return $this->belongsTo(User::class, 'friend_id');
    }

    public function format()
    {
        return [
            'id'            => $this->userFriends->id,
            'name'          => $this->userFriends->name,
            'email'         => $this->userFriends->email,
            'image'         => $this->userFriends->image,
            'accepted_at'   => $this->accepted_at
        ];
    }
}
