<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LoginResource extends JsonResource
{
    protected $token;

    public function __construct($token)
    {
        $this->token = $token;

    }

    public function toArray($request)
    {

        return [
            'success'   => true,
            "message"   => 'Successfully Logged In',
            'data'      => [
                'token' => $this->token,
            ],
        ];
    }
}
