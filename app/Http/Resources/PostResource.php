<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    protected $response;
    protected $checker;
    protected $message;

    public function __construct($response, $checker, $message = 'post.success')
    {
        $this->response = $response;
        $this->checker = $checker;
        $this->message = $message;
    }

    public function toArray($request)
    {
        if (!$this->checker) {
            if (gettype($this->response) === 'object') {
                return [
                    'success'   => false,
                    'message'   => trans('post.invalid'),
                    'errors'    => $this->response,
                ];
            }

            return [
                'success'   => true,
                'message'   => trans($this->message),
                'data'      => null
            ];
        }

        return [
            'success' => true,
            'message' => trans($this->message),
            'data'    => $this->response
        ];
    }
}
