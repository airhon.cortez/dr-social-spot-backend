<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'success'   => true,
            'message'   => trans('auth.updated'),
            'data'      => parent::toArray($request)
        ];
    }
}
