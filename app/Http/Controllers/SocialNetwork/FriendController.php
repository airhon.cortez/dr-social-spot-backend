<?php

namespace App\Http\Controllers\SocialNetWork;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\FriendRepositoryInterface;

class FriendController extends Controller
{
    private $friendRepository;

    public function __construct(FriendRepositoryInterface $friendRepository)
    {
        $this->friendRepository = $friendRepository;
    }
 
    public function index()
    {
        return $this->friendRepository->friends();
    }
}
