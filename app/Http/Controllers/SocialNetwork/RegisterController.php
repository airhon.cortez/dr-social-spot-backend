<?php

namespace App\Http\Controllers\SocialNetwork;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Http\Requests\StoreUserRequest;
use App\Http\Resources\LoginResource;
use App\Repositories\AuthRepository;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    private $authRepository;

    public function __construct(AuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;    
    }

    public function store(StoreUserRequest $request)
    {
        return (new LoginResource($this->authRepository->create($request)))
            ->response()
            ->setStatusCode(200);
    }

}
