<?php

namespace App\Http\Controllers\SocialNetwork;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePost;
use App\Http\Resources\PostResource;
use App\Repositories\Interfaces\PostRepositoryInterface;

class PostController extends Controller
{

    private $postRepository;

    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function index()
    {
        return $this->postRepository->all();
    }

    public function store(StorePost $request)
    {
        return (new PostResource($this->postRepository
            ->attributes($request)
            ->store(), true))
            ->response()
            ->setStatusCode(200);
    }

    public function show($post)
    {
        return (new PostResource($this->postRepository
            ->show($post), true))
            ->response()
            ->setStatusCode(200);
    }

    public function update(StorePost $request,$post)
    {
        return (new PostResource($this->postRepository
            ->attributes($request)
            ->update($post), true, 'post.updated'))
            ->response()
            ->setStatusCode(200);
    }

    public function destroy($post)
    {
        return (new PostResource($this->postRepository
            ->delete($post), true, 'post.deleted'))
            ->response()
            ->setStatusCode(200);
    }
}
