<?php

namespace App\Http\Controllers\SocialNetwork;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\LoginResource;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function login(LoginRequest $request)
    {

        if ($this->attemptLogin($request)) {
            return (new LoginResource($this->sendLoginResponse($request)))
                ->response()
                ->setStatusCode(200);
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }


    protected function sendLoginResponse(Request $request)
    {

        $this->clearLoginAttempts($request);

        $token = $this->guard()->user()->createToken(Str::random(40))->plainTextToken;

        return explode("|", $token)[1];
    }
}
