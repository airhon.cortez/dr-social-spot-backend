<?php

namespace App\Http\Controllers\SocialNetwork;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Resources\ProfileResource;
use App\Repositories\ProfileRepository;

class UpdatePasswordController extends Controller
{
    private $profileRepository;

    public function __construct(ProfileRepository $profileRepository)
    {
        $this->profileRepository = $profileRepository;
    }

    public function update(UpdatePasswordRequest $request)
    {
        return (new ProfileResource($this->profileRepository
            ->resetPassword($request)))
            ->response()
            ->setStatusCode(200);
    }
}
