<?php

namespace App\Http\Controllers\SocialNetwork;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostComment;
use App\Http\Resources\PostResource;
use App\Repositories\Interfaces\CommentRepositoryInterface;

class CommentPostController extends Controller
{
    private $commentRepository;

    public function __construct(CommentRepositoryInterface $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function store(StorePostComment $request, $post)
    {
        return (new PostResource($this->commentRepository
            ->comment($request)
            ->store($post), true, 'post.comment'))
            ->response()
            ->setStatusCode(200);
    }

    public function show($post, $comment)
    {
        return (new PostResource($this->commentRepository
            ->show($comment), true, ''))
            ->response()
            ->setStatusCode(200);
    }

    public function update(StorePostComment $request, $post, $comment)
    {
        return (new PostResource($this->commentRepository
            ->comment($request)
            ->update($comment), true, 'post.update_comment'))
            ->response()
            ->setStatusCode(200);
    }

    public function destroy($post, $comment)
    {
        return (new PostResource($this->commentRepository
            ->delete($comment), true, 'post.delete_comment'))
            ->response()
            ->setStatusCode(200);
    }
}
