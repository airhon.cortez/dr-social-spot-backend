<?php

namespace App\Http\Controllers\SocialNetwork;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Repositories\PostRepository;

class LikePostController extends Controller
{
    private $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function update($post)
    {
        if (auth()->user()
            ->postLikes()
            ->liked($post)
            ->first()
        ) {
            return (new PostResource($this->postRepository
                ->unlikePost($post), false, 'post.liked'))
                ->response()
                ->setStatusCode(200);
        }

        return (new PostResource($this->postRepository
            ->likePost($post), true, 'post.liked'))
            ->response()
            ->setStatusCode(200);
    }
}
