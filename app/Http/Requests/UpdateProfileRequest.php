<?php

namespace App\Http\Requests;

use App\Http\Resources\PostResource;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class UpdateProfileRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name'  => 'required|string|max:255',
            'email' => 'required|string|max:255|email|unique:users,email,' . auth()->user()->id
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException((new PostResource($validator->errors(), false))->response()->setStatusCode(422));
    }
}
