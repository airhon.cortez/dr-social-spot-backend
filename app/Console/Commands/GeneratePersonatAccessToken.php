<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class GeneratePersonatAccessToken extends Command
{

    protected $signature = 'generate:pat';

    protected $description = 'Generate Personal Access Token for the backend';

    public function handle(User $user)
    {
        $user = $user->find(1);

        dd($user->createToken('dr-social-spot')->plainTextToken);
    }
}
