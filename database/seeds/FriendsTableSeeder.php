<?php

use App\Models\Friend;
use App\Models\Post;
use Illuminate\Database\Seeder;

class FriendsTableSeeder extends Seeder
{

    public function run()
    {
        Post::get()->map(function ($post) {
            $post->comments->map(function ($comment) use ($post) {
                if (!$this->checkIfFriends($comment->user, $post->user)) {
                    Friend::create([
                        'user_id'       => $post->user->id,
                        'friend_id'     => $comment->user->id,
                        'status'        => 'accepted',
                        'accepted_at'   => now(),
                    ]);
                }
            });
        });
    }

    private function checkIfFriends($user, $user2)
    {
        $friend = (new Friend)->newQuery();

        $friend->where(function ($query) use ($user) {
            $query->where('user_id', $user->id);
            $query->orWhere('friend_id', $user->id);
        });

        $friend->where(function ($query) use ($user2) {
            $query->where('user_id', $user2->id);
            $query->orWhere('friend_id', $user2->id);
        });

        return $friend->whereStatus('accepted')->first();
    }
}
