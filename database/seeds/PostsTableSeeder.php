<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class PostsTableSeeder extends Seeder
{

    public function run()
    {
        User::get()->map(function ($user) {
            $user->posts()->create([
                'title'     => Str::random(10),
                'content'   => Str::random(30),
            ]);
        });
    }
}
