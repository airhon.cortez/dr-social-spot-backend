<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'name' => 'Airhon Carlo Cortez',
            'email' => 'cortezairhoncarlo@gmail.com',
            'password' => bcrypt('password'), 
        ]);
        
        factory(User::class, 20)->create();
    }
}
