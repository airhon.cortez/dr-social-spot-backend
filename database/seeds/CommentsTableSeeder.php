<?php

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CommentsTableSeeder extends Seeder
{
    public function run()
    {
        User::get()->map(function ($user) {
            Post::get()->map(function ($post) use ($user) {
                if ($user->id !== $post->user_id) {
                    $user->comments()->create([
                        'post_id'   => $post->id,
                        'content'   => Str::random(40),
                    ]);
                }
            });
        });
    }
}
