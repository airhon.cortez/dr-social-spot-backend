<?php

return [

    'invalid'               => 'Invalid data, please check your inputs.',
    'success'               => 'Post Created Successfully.',
    'updated'               => 'Post Updated Successfully.',
    'deleted'               => 'Post Deleted Successfully.',
    'liked'                 => 'You have liked this post.',
    'unlike'                => 'You have unliked this post.',
    'comment'               => 'Comment Sent Successfully.',
    'update_comment'        => 'Comment Updated Successfully.',
    'delete_comment'        => 'Comment Deleted Successfully.',
];
